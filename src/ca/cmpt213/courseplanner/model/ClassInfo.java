package ca.cmpt213.courseplanner.model;

import java.util.Vector;

/*
 * Stores instructors, location, and semester of a course
 */
public class ClassInfo {

	private Instructors instructors = new Instructors();
	private Location location = new Location();
	private Semester semester = new Semester();

	public Vector<ComponentInfo> components = new Vector<ComponentInfo>();

	public String getSemester() {
		return semester.getSemester();
	}
	
	public String getSeasonSemester() {
		return semester.getSeasonSemester();
	}
	
	public String getYearSemester() {
		return semester.getYearSemester();
	}

	public void setSemester(String semester) {
		this.semester.setSemester(semester);
	}

	public String getLocation() {
		return location.getLocation();
	}

	public void setLocation(String location) {
		this.location.setLocation(location);
	}

	public String getInstructors() {
		return instructors.getInstructors();
	}

	public void setInstructors(String instructors) {
		this.instructors.setInstructors(instructors);
	}

	public void addInstructors(String instructors) {
		this.instructors.addInstructors(instructors);
	}
}
