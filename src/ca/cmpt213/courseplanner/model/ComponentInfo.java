package ca.cmpt213.courseplanner.model;

public class ComponentInfo {
	private ComponentCode componentCode = new ComponentCode();
	private EnrolmentCapacity enrolCapacity = new EnrolmentCapacity();
	private EnrolmentTotal enrolTotal = new EnrolmentTotal();

	public String getEnrolCapacity() {
		return enrolCapacity.getEnrolCapacity();
	}

	public void setEnrolCapacity(String enrolCapacity) {
		this.enrolCapacity.setEnrolCapacity(enrolCapacity);
	}

	public void addEnrolCapacity(String enrolCapacity) {
		this.enrolCapacity.addEnrolCapacity(enrolCapacity);
	}

	public String getEnrolTotal() {
		return enrolTotal.getEnrolTotal();
	}

	public void setEnrolTotal(String enrolTotal) {
		this.enrolTotal.setEnrolTotal(enrolTotal);
	}

	public void addEnrolTotal(String enrolTotal) {
		this.enrolTotal.addEnrolTotal(enrolTotal);
	}

	public String getComponentCode() {
		return componentCode.getComponentCode();
	}

	public void setComponentCode(String componentCode) {
		this.componentCode.setComponentCode(componentCode);
	}
}
