package ca.cmpt213.courseplanner.model;

public class Semester {

	private String semester;
	private String seasonSemester;
	private String yearSemester;

	public Semester() {

	}

	public void setSemester(String semester) {
		this.semester = semester;
		if(semester.endsWith("1")){
			seasonSemester = "Spring";
		}
		else if(semester.endsWith("4")){
			seasonSemester = "Summer";
		} 
		else if(semester.endsWith("7")){
			seasonSemester = "Fall";
		} else {
			seasonSemester = "UNKNOWN";
		}
		this.yearSemester = "20" + semester.substring(1, 3);
	}

	public String getSemester() {
		return semester;
	}
	
	public String getSeasonSemester() {
		return seasonSemester;
	}
	
	public String getYearSemester() {
		return yearSemester;
	}
}
