package ca.cmpt213.courseplanner.model;

public class Instructors {

	private String instructors;

	public Instructors() {

	}

	public void setInstructors(String instructors) {
		if (instructors.equals("(null)")) {
			instructors = " - Unknown -";
		}
		this.instructors = instructors;
	}

	public String getInstructors() {
		return instructors;
	}

	public void addInstructors(String instructors) {
		if (instructors.equals("(null)")) {
			instructors = " - Unknown -";
		}
		if (!this.instructors.toLowerCase().contains(instructors.toLowerCase())) {
			this.instructors = this.instructors + ", " + instructors;
		}
	}
}
