package ca.cmpt213.courseplanner.model;

public class ComponentCode {
	private String componentCode;

	public ComponentCode() {

	}

	public void setComponentCode(String componentCode) {
		this.componentCode = componentCode;
	}

	public String getComponentCode() {
		return componentCode;
	}
}
