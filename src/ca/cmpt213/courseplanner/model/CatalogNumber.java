package ca.cmpt213.courseplanner.model;

public class CatalogNumber {

	private String catalogNumber;

	public CatalogNumber() {

	}

	public void setCatalogNumber(String catalogNumber) {
		this.catalogNumber = catalogNumber;
	}

	public String getCatalogNumber() {
		return catalogNumber;
	}
}
