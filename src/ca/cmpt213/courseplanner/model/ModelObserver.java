package ca.cmpt213.courseplanner.model;

/*
 * Interface for observers to implement to be able to observe
 * changed to CourseListFilterUI objects.
 */
public interface ModelObserver {
	void stateChanged();
}
