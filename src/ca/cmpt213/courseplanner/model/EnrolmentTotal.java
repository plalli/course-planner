package ca.cmpt213.courseplanner.model;

public class EnrolmentTotal {

	private String enrolTotal;

	public EnrolmentTotal() {

	}

	public void setEnrolTotal(String enrolTotal) {
		this.enrolTotal = enrolTotal;
	}

	public String getEnrolTotal() {
		return enrolTotal;
	}

	public void addEnrolTotal(String enrolTotal) {
		this.enrolTotal = Integer.toString(Integer.parseInt(this.enrolTotal)
				+ Integer.parseInt(enrolTotal));
	}
}
