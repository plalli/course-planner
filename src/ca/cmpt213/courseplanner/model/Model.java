package ca.cmpt213.courseplanner.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.Vector;

/*
 * Model holds all back end information which include courses, and other details.
 * Also holds, the observers.
 */

public class Model {
	private Vector<Subject> subjects = new Vector<Subject>();
	private Vector<ModelObserver> courseListObs = new Vector<ModelObserver>();
	private Vector<ModelObserver> courseOfferingsObs = new Vector<ModelObserver>();
	private Vector<ModelObserver> courseDetailsObs = new Vector<ModelObserver>();
	private int subjectSelected;
	private int courseNum = -1;
	private boolean undergradClasses;
	private boolean gradClasses;
	private ClassInfo infoForCourse;
	
	public final static String fileOfCourseData = "/data/course_data_2015.csv";

	public Model() throws FileNotFoundException {
		loadData();
		makeFile();
	}

	public Vector<Subject> getSubjects() {
		return subjects;
	}

	private void loadData() throws FileNotFoundException {
		Scanner scanFile = new Scanner(new File(System.getProperty("user.dir")
				+ fileOfCourseData));

		String line = scanFile.nextLine(); // Removes unneeded initial line
		String[] splitLine;
		while (scanFile.hasNextLine()) {
			line = scanFile.nextLine();
			// REFERENCE:
			// http://stackoverflow.com/questions/1757065/java-splitting-a-comma-separated-string-but-ignoring-commas-in-quotes
			splitLine = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

			// Does subject exist? If not, create it and add to vector
			boolean check = false;
			for (Subject subject : subjects) {
				if (subject.getSubject().equals(splitLine[1])) {
					check = true;
				}
			}
			if (!check) {
				Subject subject = new Subject();
				subject.setSubject(splitLine[1]);
				subjects.add(subject);
			}

			// Does class exist? If not, create it create and add to vector
			for (Subject subject : subjects) {
				if (subject.getSubject().equals(splitLine[1])) {
					check = false;
					for (Course course : subject.courses) {
						if (course.getCatalogNumber().equals(splitLine[2])) {
							check = true;
						}
					}
					if (!check) {
						Course newCourse = new Course();
						newCourse.setCatalogNumber(splitLine[2]);
						subject.courses.add(newCourse);
					}
				}
			}

			// Go through current list of subjects
			for (Subject subject : subjects) {
				// Check if current subject is the right subject
				if (subject.getSubject().equals(splitLine[1])) {
					// Go through list of courses in subject
					for (Course course : subject.courses) {
						// Check if current course number is the right number
						if (course.getCatalogNumber().equals(splitLine[2])) {

							// Check if classinfo exists
							check = false;
							for (ClassInfo newClassInfo : course.classes) {
								if (newClassInfo.getSemester().equals(
										splitLine[0]) && newClassInfo.getLocation().equals(splitLine[3])) {
									check = true;
									if (!newClassInfo.getInstructors().equals(
											splitLine[6])) {
										newClassInfo
												.addInstructors(splitLine[6]);
									}
								}
							}

							if (!check) {
								ClassInfo newClassInfo = new ClassInfo();
								newClassInfo.setSemester(splitLine[0]);
								newClassInfo.setLocation(splitLine[3]);
								newClassInfo.setInstructors(splitLine[6]);
								course.classes.add(newClassInfo);
							}

							for (ClassInfo newClassInfo : course.classes) {
								if (newClassInfo.getSemester().equals(
										splitLine[0]) && newClassInfo.getLocation().equals(splitLine[3])) {

									// Check if component exists
									check = false;
									for (ComponentInfo newComponent : newClassInfo.components) {
										if (newComponent.getComponentCode()
												.equals(splitLine[7])) {
											check = true;
											newComponent
													.addEnrolCapacity(splitLine[4]);
											newComponent
													.addEnrolTotal(splitLine[5]);
										}
									}

									if (!check) {
										ComponentInfo newComponent = new ComponentInfo();
										newComponent
												.setEnrolCapacity(splitLine[4]);
										newComponent
												.setEnrolTotal(splitLine[5]);
										newComponent
												.setComponentCode(splitLine[7]);
										newClassInfo.components
												.add(newComponent);
									}
								}
							}
						}
					}
				}
			}
		}

		Collections.sort(subjects, new Comparator<Subject>() {
			@Override
			public int compare(Subject sub1, Subject sub2) {
				String subject1 = sub1.getSubject();
				String subject2 = sub2.getSubject();
				return subject1.compareTo(subject2);
			}
		});

		for (Subject subject : subjects) {
			Collections.sort(subject.courses, new Comparator<Course>() {
				@Override
				public int compare(Course course1, Course course2) {
					String courseA = course1.getCatalogNumber();
					String courseB = course2.getCatalogNumber();
					return courseA.compareTo(courseB);
				}
			});
		}

		for (Subject subject : subjects) {
			for (Course course : subject.courses) {
				for (ClassInfo classinfo : course.classes) {
					Collections.sort(classinfo.components,
							new Comparator<ComponentInfo>() {
								@Override
								public int compare(ComponentInfo component1,
										ComponentInfo component2) {
									String componentA = component1
											.getComponentCode();
									String componentB = component2
											.getComponentCode();
									return componentA.compareTo(componentB);
								}
							});
				}
			}
		}

		scanFile.close();
	}

	private void makeFile() {
		PrintWriter writer = null;
		String textFile = "output_dump.txt";
		try {
			writer = new PrintWriter(textFile, "UTF-8");
			for (Subject subject : subjects) {
				for (Course course : subject.courses) {
					writer.println(subject.getSubject() + " "
							+ course.getCatalogNumber());
					for (ClassInfo classinfo : course.classes) {
						writer.println("      " + classinfo.getSemester()
								+ " in " + classinfo.getLocation() + " by "
								+ classinfo.getInstructors());
						for (ComponentInfo componentInfo : classinfo.components) {
							writer.println("            Type="
									+ componentInfo.getComponentCode()
									+ ", Enrollment="
									+ componentInfo.getEnrolTotal() + "/"
									+ componentInfo.getEnrolCapacity());
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			writer.close();
		}
	}

	/*
	 * Functions to support being observable.
	 */
	public void addCourseListObs(ModelObserver observer) {
		courseListObs.add(observer);
	}
	
	public void addCourseOfferingsObs(ModelObserver observer) {
		courseOfferingsObs.add(observer);
	}
	
	public void addCourseDetailsObs(ModelObserver observer) {
		courseDetailsObs.add(observer);
	}

	private void notifyCourseList() {
		for (ModelObserver observer : courseListObs) {
			observer.stateChanged();
		}
	}
	
	private void notifyCourseOfferings() {
		for (ModelObserver observer : courseOfferingsObs) {
			observer.stateChanged();
		}
	}

	public void setSubjectSelected(int subjectSelected) {
		this.subjectSelected = subjectSelected;
		notifyCourseList();
	}

	public int getSubjectSelected() {
		return subjectSelected;
	}

	public Subject getSubjectForCourse() {
		return subjects.get(subjectSelected);
	}

	public void setCourseNumberSelected(int courseNum) {
		this.courseNum = courseNum;
		notifyCourseOfferings();
	}

	public int getCourseNumber() {
		return courseNum;
	}

	public void setGradUndergrad(boolean undergrad, boolean grad) {
		undergradClasses = undergrad;
		gradClasses = grad;
	}
	
	public boolean getUndergrad() {
		return undergradClasses;
	}
	
	public boolean getGrad() {
		return gradClasses;
	}

	public void setCourseOffering(ClassInfo info) {
		this.infoForCourse = info;
		notifyCourseDetails();
	}
	
	public ClassInfo getCourseDetail() {
		return infoForCourse;
	}

	private void notifyCourseDetails() {
		for (ModelObserver observer : courseDetailsObs) {
			observer.stateChanged();
		}
	}
	
	
}
