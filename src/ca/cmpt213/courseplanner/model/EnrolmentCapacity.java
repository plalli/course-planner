package ca.cmpt213.courseplanner.model;

public class EnrolmentCapacity {

	private String enrolCapacity;

	public EnrolmentCapacity() {

	}

	public void setEnrolCapacity(String enrolCapacity) {
		this.enrolCapacity = enrolCapacity;
	}

	public String getEnrolCapacity() {
		return enrolCapacity;
	}

	public void addEnrolCapacity(String enrolCapacity) {
		this.enrolCapacity = Integer
				.toString(Integer.parseInt(this.enrolCapacity)
						+ Integer.parseInt(enrolCapacity));
	}
}
