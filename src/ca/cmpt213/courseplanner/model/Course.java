package ca.cmpt213.courseplanner.model;

import java.util.Vector;

/*
 * Stores all the classes for specific subject in order to
 * get course offerings.
 */

public class Course {
	private CatalogNumber catalogNumber = new CatalogNumber();
	public Vector<ClassInfo> classes = new Vector<ClassInfo>();

	public String getCatalogNumber() {
		return catalogNumber.getCatalogNumber();
	}

	public void setCatalogNumber(String catalogNumber) {
		this.catalogNumber.setCatalogNumber(catalogNumber);
	}
}
