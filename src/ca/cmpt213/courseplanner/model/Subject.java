package ca.cmpt213.courseplanner.model;

import java.util.Vector;

public class Subject {

	private String subject;

	public Vector<Course> courses = new Vector<Course>();

	public Subject() {

	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}
}
