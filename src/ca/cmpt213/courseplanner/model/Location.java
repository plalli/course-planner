package ca.cmpt213.courseplanner.model;

public class Location {

	private String location;

	public Location() {

	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}
}
