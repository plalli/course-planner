package ca.cmpt213.courseplanner.ui;

import java.awt.BorderLayout;
import java.io.FileNotFoundException;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ca.cmpt213.courseplanner.model.Model;
import ca.cmpt213.courseplanner.model.ModelObserver;

/*
 * Main class that initially calls the model, and UI for course 
 * planner.
 */

@SuppressWarnings("serial")
public class MainGUI extends JFrame {
	private static Model model;
	private static JFrame mainFrame = new JFrame("FAS Course Planner");
	
	public static void main(String[] args) throws FileNotFoundException {
		String a = Model.fileOfCourseData;
		boolean fileFound = true;
		try{
			model = new Model();
		} catch(FileNotFoundException e){
			fileFound = false;
			JOptionPane.showMessageDialog(new JFrame(), "Data file (" + System.getProperty("user.dir")
					+ a + ") not found.");
		}
		if(fileFound){
			makePanels();
		}
	}

	private static void makePanels() {
		JPanel westPanel = new JPanel();
		JPanel centerPanel = new JPanel();
		JPanel eastPanel = new JPanel();
		
		mainFrame.setLayout(new BorderLayout());
		westPanel.setLayout(new BoxLayout(westPanel, BoxLayout.PAGE_AXIS));
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS));
		eastPanel.setLayout(new BoxLayout(eastPanel, BoxLayout.PAGE_AXIS));
		
		westPanel.add(makeCourseListFilter(), BorderLayout.NORTH);
		westPanel.add(makeCourseList(), BorderLayout.CENTER);
		centerPanel.add(courseOfferingsBySem(), BorderLayout.CENTER);
		eastPanel.add(makeStatistics(), BorderLayout.NORTH);
		eastPanel.add(CourseOfferingDetails(), BorderLayout.CENTER);
		
		mainFrame.add(westPanel, BorderLayout.WEST);
		mainFrame.add(centerPanel, BorderLayout.CENTER);
		
		mainFrame.add(eastPanel, BorderLayout.EAST);
		
		registerAsObserver();
		
		mainFrame.pack();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
	}

	private static void registerAsObserver() {
		model.addCourseListObs(new ModelObserver() {
			@Override
			public void stateChanged() {
				updatePanels();
			}
		});
		model.addCourseOfferingsObs(new ModelObserver() {
			@Override
			public void stateChanged() {
				updatePanels();
			}
		});
		model.addCourseDetailsObs(new ModelObserver() {
			@Override
			public void stateChanged() {
				updatePanels();
			}
		});
	}
	
	private static void updatePanels() {
		mainFrame.revalidate();
		mainFrame.pack();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
	}

	private static ABCPanel makeCourseListFilter() {
		CourseListFilterUI courseListFilter = new CourseListFilterUI(model, "Course List Filter");
		return courseListFilter;
	}
	
	private static ABCPanel makeCourseList() {
		CourseListUI courseList = new CourseListUI(model, "Course List");
		return courseList;
	}
	
	private static ABCPanel courseOfferingsBySem() {
		CourseOfferingsBySemester courseOfferings = new CourseOfferingsBySemester(model, "Course Offerings by Semester");
		return courseOfferings;
	}
	
	private static ABCPanel makeStatistics() {
		Statistics stats = new Statistics(model, "Statistics");
		return stats;
	}
	
	private static ABCPanel CourseOfferingDetails() {
		CourseOfferingDetails courseDetails = new CourseOfferingDetails(model, "Details of Course Offering");
		return courseDetails;
	}
}