package ca.cmpt213.courseplanner.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ca.cmpt213.courseplanner.model.Model;
import ca.cmpt213.courseplanner.model.Subject;

/*
 * Adds components to course lift filter panel which inherits ABC panel
 * in order to add panel to UI.
 */

@SuppressWarnings("serial")
public class CourseListFilterUI extends ABCPanel {
	private Model model;
	private JPanel panel;
	private JComboBox<String> myComboBox;
	private Vector<String> options;
	private JCheckBox undergradCourses;
	private JCheckBox gradCourses;
	private JButton updateList;
	
	public CourseListFilterUI(Model model, String title) {
		super(model);
		this.model = model;
		add(makeLabel(title), BorderLayout.NORTH);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(275, 140));
		options = new Vector<String>();
		undergradCourses = new JCheckBox("Include undergrad courses", true);
		gradCourses = new JCheckBox("Include grad courses", false);
		updateListButton();
		addOptions();
		myComboBox = new JComboBox<String>(options);
		
		addItemsToPanel();
	}

	private void addItemsToPanel() {
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		panel.add(new JLabel("Department"));
		panel.add(myComboBox);
		panel.add(undergradCourses);
		panel.add(gradCourses);
		panel.add(updateList);
		add(userContents(panel), BorderLayout.CENTER);
	}

	private void updateListButton() {
		updateList = new JButton("Update Course List");
		updateList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int subjectSelected = myComboBox.getSelectedIndex();
				model.setGradUndergrad(undergradCourses.isSelected(), gradCourses.isSelected());
				model.setCourseNumberSelected(-1);
				model.setSubjectSelected(subjectSelected);
				model.setCourseOffering(null);
			}
		});
	}

	private void addOptions() {
		for (Subject courses : model.getSubjects()) {
			options.add(courses.getSubject());
		}
	}

	@Override
	protected Model getModel() {
		return model;
	}
}