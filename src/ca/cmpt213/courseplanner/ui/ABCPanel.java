package ca.cmpt213.courseplanner.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import ca.cmpt213.courseplanner.model.Model;

/*
 * Abstract base class panel that is inherited by all the separate panels such as course offerings
 * and statistics and is outputted to the UI. This includes a title, and user contents.
 */

@SuppressWarnings("serial")
public abstract class ABCPanel extends JPanel {
	private Model model;

	public ABCPanel(Model model) {
		this.model = model;
		setLayout(new BorderLayout());
	}
	
	public Component makeLabel(String title) {
		JLabel label = new JLabel("  " + title);
		label.setForeground(Color.blue);
		return label;
	}
	
	public Component userContents(JPanel userPanel) {
		BevelBorder colorBorder = (BevelBorder) BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.black, Color.gray);
		Border thickBorder = new LineBorder(userPanel.getBackground(), 5);
		userPanel.setBorder(BorderFactory.createCompoundBorder(thickBorder, colorBorder));
		getPreferredSize(userPanel);
		return userPanel;
	}
	
	private Component getPreferredSize(JPanel panel) {
		Dimension prefSize = panel.getPreferredSize();
		Dimension newSize = new Dimension(
				Integer.MAX_VALUE,
				(int)prefSize.getHeight());
		panel.setMaximumSize(newSize);
		return panel;
	}

	protected abstract Model getModel();
}
