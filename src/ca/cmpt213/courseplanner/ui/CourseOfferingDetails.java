package ca.cmpt213.courseplanner.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ca.cmpt213.courseplanner.model.ClassInfo;
import ca.cmpt213.courseplanner.model.ComponentInfo;
import ca.cmpt213.courseplanner.model.Model;

/*
 * Panel that displays details of an offered course that is clicked.
 */

@SuppressWarnings("serial")
public class CourseOfferingDetails extends ABCPanel {
	private Model model;
	private JPanel panel;
	
	public CourseOfferingDetails(Model model, String title) {
		super(model);
		this.model = model;
		add(makeLabel(title), BorderLayout.NORTH);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(275, 170));
		add(userContents(panel), BorderLayout.CENTER);
		
		displayCourseDetails();
	}

	private void displayCourseDetails() {
		model.addCourseDetailsObs(
				() -> updateCourseDetails());
	}

	private void updateCourseDetails() {
		panel.removeAll();
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(275, 170));
		panel.setLayout(new GridLayout(2, 2));
		
		if (model.getCourseDetail() == null) {
			JPanel titles = new JPanel();
			titles.setLayout(new BoxLayout(titles, BoxLayout.Y_AXIS));
			titles.add(new JLabel("Course Name:"));
			titles.add(new JLabel("Semester:"));
			titles.add(new JLabel("Location:"));
			titles.add(new JLabel("Instructors:"));
			panel.add(titles);

			JPanel courseInfo = new JPanel();
			courseInfo.setLayout(new BoxLayout(courseInfo, BoxLayout.Y_AXIS));
			courseInfo.setBackground(Color.WHITE);
			panel.add(courseInfo);

			JPanel sectionType = new JPanel();
			panel.add(sectionType);

			JPanel enrollment = new JPanel();
			panel.add(enrollment);
		} else {
			ClassInfo info = model.getCourseDetail();
			String courseName = model.getSubjectForCourse().getSubject()
					+ " "
					+ model.getSubjectForCourse().courses.get(
							model.getCourseNumber()).getCatalogNumber();

			JPanel titles = new JPanel();
			titles.setLayout(new BoxLayout(titles, BoxLayout.Y_AXIS));
			titles.add(new JLabel("Course Name:"));
			titles.add(new JLabel("Semester:"));
			titles.add(new JLabel("Location:"));
			titles.add(new JLabel("Instructors:"));
			panel.add(titles);

			JPanel courseInfo = new JPanel();
			courseInfo.setLayout(new BoxLayout(courseInfo, BoxLayout.Y_AXIS));
			courseInfo.setBackground(Color.WHITE);
			courseInfo.add(new JLabel(courseName));
			courseInfo.add(new JLabel(info.getSemester()));
			courseInfo.add(new JLabel(info.getLocation()));
			if(!info.getInstructors().equals(" - Unknown -")){
				courseInfo.add(new JLabel("<html>" + info.getInstructors()
						+ "</html>"));	
			}
			panel.add(courseInfo);

			JPanel sectionType = new JPanel();
			sectionType.setLayout(new BoxLayout(sectionType, BoxLayout.Y_AXIS));
			sectionType.add(new JLabel("Section Type"));
			for (ComponentInfo sections : info.components) {
				sectionType.add(new JLabel(sections.getComponentCode()));
			}
			panel.add(sectionType);

			JPanel enrollment = new JPanel();
			enrollment.setLayout(new BoxLayout(enrollment, BoxLayout.Y_AXIS));
			enrollment.add(new JLabel("Enrollment (filled/cap)"));
			for (ComponentInfo sections : info.components) {
				enrollment.add(new JLabel(sections.getEnrolTotal() + "/"
						+ sections.getEnrolCapacity()));
			}
			panel.add(enrollment);
		}
		
		add(userContents(panel), BorderLayout.CENTER);
	}

	@Override
	protected Model getModel() {
		return model;
	}

}
