package ca.cmpt213.courseplanner.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ca.cmpt213.courseplanner.model.ClassInfo;
import ca.cmpt213.courseplanner.model.Course;
import ca.cmpt213.courseplanner.model.Model;

@SuppressWarnings("serial")
public class Statistics extends ABCPanel {
	private Model model;
	private JPanel panel;
	
	public Statistics(Model model, String title) {
		super(model);
		this.model = model;
		add(makeLabel(title), BorderLayout.NORTH);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(275, 475));
		add(userContents(panel), BorderLayout.CENTER);
		
		updateCourseForStats();
	}

	private void displayStatistics() {
		panel.removeAll();
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(275, 475));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JLabel courseTitle = new JLabel();
		if(model.getCourseNumber() == -1){
			courseTitle.setText("Course:");
		} else {
			courseTitle.setText("Course: " + model.getSubjectForCourse().getSubject() + " " +
					model.getSubjectForCourse().courses.get(model.getCourseNumber()).getCatalogNumber());
		}
		panel.add(courseTitle);
		panel.add(Box.createRigidArea(new Dimension(15,0)));

		panel.add(new JLabel("Semester Offerings:"));
		
		String[] semesters = new String[3];
		semesters[0] = "Spring";
		semesters[1] = "Summer";
		semesters[2] = "Fall";
		
		int[] classesPerSem = new int[3];
		if (!(model.getCourseNumber() == -1)) {
			Course currentCourse = model.getSubjectForCourse().courses.get(model.getCourseNumber());
			for (int i = 0; i < 3; i++) {
				int classCount = 0;
				for (ClassInfo classinfo : currentCourse.classes) {
					if (classinfo.getSeasonSemester().equals(semesters[i])) {
						classCount++;
					}
				}
				classesPerSem[i] = classCount;
			}
		}
		else{
			for (int i = 0; i < 3; i++) {
				classesPerSem[i] = 0;
			}
		}
		
		BarGraphModel graphSemester = new BarGraphModel(classesPerSem, semesters);
		BarGraphIcon graphSemesterPic = new BarGraphIcon(graphSemester, 260, 200);
		JLabel semGraph = new JLabel(graphSemesterPic);
		panel.add(semGraph);
		
		panel.add(Box.createRigidArea(new Dimension(15,0)));
		
		panel.add(new JLabel("Campus Offerings:"));
		
		String[] locationsForCheck = new String[3];
		locationsForCheck[0] = "BURNABY";
		locationsForCheck[1] = "SURREY";
		locationsForCheck[2] = "HRBRCNTR";
		
		String[] locations = new String[4];
		locations[0] = "Bby";
		locations[1] = "Sry";
		locations[2] = "Van";
		locations[3] = "Other";
		
		int[] classesPerLocation = new int[4];
		if (!(model.getCourseNumber() == -1)) {
			Course currentCourse = model.getSubjectForCourse().courses.get(model.getCourseNumber());
			for (int i = 0; i < 4; i++) {
				int classCount = 0;
				for (ClassInfo classinfo : currentCourse.classes) {
					if(i == 3){
						if (!classinfo.getLocation().equals(locationsForCheck[0]) && !classinfo.getLocation().equals(locationsForCheck[1]) && !classinfo.getLocation().equals(locationsForCheck[2])) {
							classCount++;
						}
					}
					else if (classinfo.getLocation().equals(locationsForCheck[i])) {
						classCount++;
					}
				}
				classesPerLocation[i] = classCount;
			}
		}
		else{
			for (int i = 0; i < 4; i++) {
				classesPerLocation[i] = 0;
			}
		}
		
		BarGraphModel graphLocation = new BarGraphModel(classesPerLocation, locations);
		BarGraphIcon graphLocationPic = new BarGraphIcon(graphLocation, 260, 200);
		JLabel locationGraph = new JLabel(graphLocationPic);
		panel.add(locationGraph);
		
		add(userContents(panel), BorderLayout.CENTER);
	}

	@Override
	protected Model getModel() {
		return model;
	}
	
	private void updateCourseForStats() {
		model.addCourseOfferingsObs(
				() -> displayStatistics());
	}
}
