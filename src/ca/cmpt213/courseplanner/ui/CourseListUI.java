package ca.cmpt213.courseplanner.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ca.cmpt213.courseplanner.model.Course;
import ca.cmpt213.courseplanner.model.Model;

/* 
 * Displays course list panel which outputs the list of classes
 * for a specific subject. This includes grad/undergrad classes.
 */

@SuppressWarnings("serial")
public class CourseListUI extends ABCPanel {
	private Model model;
	private String title;
	private JPanel panel;
	private JList<String> listOfCourses;
	private Vector<String> keepingTrackOfCourses;

	public CourseListUI(Model model, String title) {
		super(model);
		this.model = model;
		this.title = title;
		add(makeLabel(title), BorderLayout.NORTH);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(275, 460));
		add(userContents(panel), BorderLayout.CENTER);
		
		displayCourseList();
		panel.setBackground(Color.white);
		panel.setOpaque(true);
	}

	private void displayCourseList() {
		model.addCourseListObs(
				() -> updateCourseList());
	}
	
	private void updateCourseList() {
		Vector<String> courseNums = getCourseList();
		
		listOfCourses = new JList<String>(courseNums);
		listOfCourses.setFixedCellWidth(100);
		listOfCourses.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listOfCourses.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		listOfCourses.setVisibleRowCount(-1);
		courseListListener();
		
		JScrollPane listScroller = new JScrollPane(listOfCourses);
		listScroller.setPreferredSize(new Dimension(260, 440));
		panel.removeAll();
		panel.add(listScroller);
		removeAll();
		add(makeLabel(title), BorderLayout.NORTH);
		add(userContents(panel), BorderLayout.CENTER);
	}

	private Vector<String> getCourseList() {
		boolean undergradChecked = model.getUndergrad();
		boolean gradChecked = model.getGrad();
		Vector<String> courseNums = new Vector<String>();
		keepingTrackOfCourses = new Vector<String>();
		String courseName = model.getSubjectForCourse().getSubject();
		
		for (Course courses : model.getSubjectForCourse().courses) {
			String courseNumber = courses.getCatalogNumber();
			int courseNumInInt = Integer.parseInt(courseNumber.replaceAll("[\\D]", ""));
		
			if (undergradChecked == true && gradChecked == true) {
				courseNums.add(courseName + " " + courseNumber);
				
			} else if (undergradChecked == true && gradChecked == false) {
				if (courseNumInInt < 500) {
					courseNums.add(courseName + " " + courseNumber);
				}
				
			} else if (undergradChecked == false && gradChecked == true) {
				if (courseNumInInt >= 500) {
					courseNums.add(courseName + " " + courseNumber);
				}
			}
			keepingTrackOfCourses.add(courseName + " " + courseNumber);
		}
		return courseNums;
	}

	private void courseListListener() {
		listOfCourses.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				String courseName = listOfCourses.getSelectedValue();
				int courseNum = keepingTrackOfCourses.indexOf(courseName);
				model.setCourseNumberSelected(courseNum);
				model.setCourseOffering(null);
			}
		});
	}

	@Override
	protected Model getModel() {
		return model;
	}
}
