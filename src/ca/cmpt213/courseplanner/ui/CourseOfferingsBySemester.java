package ca.cmpt213.courseplanner.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ca.cmpt213.courseplanner.model.ClassInfo;
import ca.cmpt213.courseplanner.model.Course;
import ca.cmpt213.courseplanner.model.Model;

/*
 * Panel that displays grid, and buttons for specific classes offered
 * from course list.
 */

@SuppressWarnings("serial")
public class CourseOfferingsBySemester extends ABCPanel {
	private Model model;
	private String title;
	private JPanel panel;
	private int vectorIdx;
	
	public enum Semester {
		SPRING, SUMMER, FALL;
	}
	
	public CourseOfferingsBySemester(Model model, String title) {
		super(model);
		this.model = model;
		this.title = title;
		add(makeLabel(title), BorderLayout.NORTH);
		
		panel = new JPanel();
		panel.add(new JLabel("Use a filter to select a course."));
		panel.setPreferredSize(new Dimension(600, 600));
		add(userContents(panel), BorderLayout.CENTER);
		
		displayCourseOfferings();
	}

	private void displayCourseOfferings() {
		model.addCourseOfferingsObs(
				() -> updateCourseOfferings());
	}

	private void updateCourseOfferings() {
		JPanel pane = new JPanel(new GridBagLayout());
		pane.setPreferredSize(new Dimension(600, 600));
		GridBagConstraints c = new GridBagConstraints();
	
		if (model.getCourseNumber() != -1) {
			Course course = model.getSubjectForCourse().courses.get(model.getCourseNumber());
			Vector<String> years = new Vector<String>();
			Vector<String> coordinates = new Vector<String>();
			Vector<ClassInfo> classInfo = new Vector<ClassInfo>();
			
			int y = 1;
			for (ClassInfo info : course.classes) {
				for (int x = 0; x <= 3; x++) {
					if (x == 0 && !years.contains(info.getYearSemester())) {
						years.add(info.getYearSemester());
						y = years.size();
					} else if (x != 0) {
						getLocationOfButtons(info, x, y, coordinates, classInfo);
					}
				}
			}
			
			c.gridx = 0;
			c.gridy = 0;
			c.weighty = 1;
			c.fill = GridBagConstraints.BOTH;
			// Adds semester labels
			pane.add(new JLabel());
			for (Semester sem : Semester.values()) {
				pane.add(new JLabel(sem.name()));
			}
			
			vectorIdx = 0;
			int yAxis = 1;
			int index = 0;
			for (String year : years) {
				for (int x = 0; x <= 3; x++) {
					c.gridx = x;
					c.gridy = yAxis;
					c.weighty = 1;
					c.fill = GridBagConstraints.BOTH;
					if (x == 0) {
						index++;
						c.weightx = 0;
						yAxis = index;
						c.gridy = yAxis;
						pane.add(new JLabel(" " + year + "  "), c);
					} else if (x != 0) {
						JPanel innerPan = new JPanel(new GridBagLayout());
						innerPan.setBorder(BorderFactory
								.createLineBorder(Color.BLACK));
						GridBagConstraints b = new GridBagConstraints();
						b.gridy = 0;
						placeButton(pane, c, x, yAxis, coordinates, classInfo, innerPan , b);
					}
				}
			}
		} else {
			pane.add(new JLabel("Use a filter to select a course."));
		}
		removeAll();
		add(makeLabel(title), BorderLayout.NORTH);
		add(userContents(pane), BorderLayout.CENTER);
	}
	
	private void getLocationOfButtons(ClassInfo info, int x, int y, Vector<String> coordinates, 
			Vector<ClassInfo> classInfo) {
		
		if (x == 1 && info.getSeasonSemester().equals("Spring")) {
			classInfo.add(info);
			coordinates.add(x + " " + y);
		} else if (x == 2 && info.getSeasonSemester().equals("Summer")) {
			classInfo.add(info);
			coordinates.add(x + " " + y);
		} else if (x == 3 && info.getSeasonSemester().equals("Fall")) {
			classInfo.add(info);
			coordinates.add(x + " " + y);
		}
	}

	private void placeButton(JPanel pane, GridBagConstraints c, int x, int y, 
			Vector<String> coordinates, Vector<ClassInfo> classInfo, JPanel innerPan, GridBagConstraints b) {
		String location = "";
		JButton courseButton;
		String courseName = model.getSubjectForCourse().getSubject() + " " +
				model.getSubjectForCourse().courses.get(model.getCourseNumber()).getCatalogNumber();
		String[] coords = coordinates.get(vectorIdx).split(" ");
		int xCoord = Integer.parseInt(coords[0]);
		int yCoord = Integer.parseInt(coords[1]);
		
		b.gridx = 0;
		b.weightx = 1;
		b.weighty = 1;
		b.fill = GridBagConstraints.BOTH;
		
		c.weightx = 1; 
		
		if (x == xCoord && y == yCoord) {
			location = classInfo.get(vectorIdx).getLocation();
			courseButton = setupButton(location, courseName, classInfo.get(vectorIdx));
			innerPan.add(courseButton, b);
			if (vectorIdx < coordinates.size() - 1) {
				vectorIdx++;
				if (b.gridy == 0) {
					b.gridy = 1;
				} else if (b.gridy == 1) {
					b.gridy = 2;
				} else if (b.gridy == 2) {
					b.gridy = 3;
				}
				placeButton(pane, c, x, y, coordinates, classInfo, innerPan, b);
			}
		}
		pane.add(innerPan, c);
	}

	private JButton setupButton(String location, String courseName, ClassInfo info) {
		JButton courseButton;
		courseButton = new JButton(courseName + " - " + location);
		
		courseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.setCourseOffering(info);
			}
		});
		return courseButton;
	}

	@Override
	protected Model getModel() {
		return model;
	}
}
